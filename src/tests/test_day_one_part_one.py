"""Day 1 Part 1 feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)

from day_one_part_one import report_repair


@scenario('features/day1_part1.feature', 'find the two entries that sum to 2020')
def test_find_the_two_entries_that_sum_to_2020():
    """find the two entries that sum to 2020."""


@given(parsers.parse('We have a starting {sequence:s}'), target_fixture="challenge_input" )
def we_have_a_starting_sequence(sequence):
    """We have a starting <sequence>."""
    return sequence


@when('I will find the two entries that sum to 2020',target_fixture="report_repair_result")
def i_will_find_the_two_entries_that_sum_to_2020(challenge_input):
    """I will find the two entries that sum to 2020."""
    return report_repair(challenge_input)


@then(parsers.parse('I will get a {result:d}'))
def i_will_get_a_result(report_repair_result, result):
    """I will get a <result>."""
    assert report_repair_result == result

