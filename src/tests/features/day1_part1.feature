
Feature: Day 1 Part 1
    A Report Repair. Before you leave, the Elves in accounting just need you to fix your expense report (your puzzle input); apparently, something isn't quite adding up.
    Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.

    Scenario Outline: find the two entries that sum to 2020
        Given We have a starting <sequence>
        
        When I will find the two entries that sum to 2020
        
        Then I will get a <result>

        Examples:
        | sequence  | result |
        | 1721+299  | 2020   |
        |           |        |
        